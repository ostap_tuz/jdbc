import java.sql.*;
import java.util.ResourceBundle;

public class ConnectionManager {
    private static Connection connection;
    private static ResourceBundle  configs = ResourceBundle.getBundle("config");

        ConnectionManager() throws SQLException { }

        public static Connection getConnection(){
            if (connection == null){
                try {
                    connection = DriverManager.getConnection(configs.getString("url"),configs.getString("user"), configs.getString("password"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return connection;
        }

}
