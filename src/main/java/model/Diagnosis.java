package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

@Table(name = "diagnosis")
public class Diagnosis {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private int id;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "recomended_medicines", length = 10000)
    private String recomendedMedicines;

    public Diagnosis(){}

    public Diagnosis(int id, String name, String recomendedMedicines) {
        this.id = id;
        this.name = name;
        this.recomendedMedicines = recomendedMedicines;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecomendedMedicines() {
        return recomendedMedicines;
    }

    public void setRecomendedMedicines(String recomendedMedicines) {
        this.recomendedMedicines = recomendedMedicines;
    }

    @Override
    public String toString() {
        return "Diagnosis{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", recomendedMedicines='" + recomendedMedicines + '\'' +
                '}';
    }
}
