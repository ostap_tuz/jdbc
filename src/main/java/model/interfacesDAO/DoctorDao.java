package model.interfacesDAO;

import model.Doctor;

public interface DoctorDao extends GeneralDao<Doctor, Integer> {
}
