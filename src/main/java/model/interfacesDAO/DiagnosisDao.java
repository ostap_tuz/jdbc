package model.interfacesDAO;

import model.Diagnosis;

public interface DiagnosisDao extends GeneralDao<Diagnosis, Integer>{

}
