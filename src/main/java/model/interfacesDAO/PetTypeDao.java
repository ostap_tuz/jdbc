package model.interfacesDAO;

import model.PetType;

public interface PetTypeDao extends GeneralDao<PetType, String> {
}
