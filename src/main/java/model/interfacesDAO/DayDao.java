package model.interfacesDAO;

import model.Day;

public interface DayDao extends GeneralDao<Day, Integer>{
}
