package model.interfacesDAO;

import model.PetMedicalCard;

public interface PetMedicalCardDao extends GeneralDao<PetMedicalCard, Integer> {
}
