package model.interfacesDAO;

import model.Client;

public interface ClientDao extends GeneralDao<Client, Integer> {
}
