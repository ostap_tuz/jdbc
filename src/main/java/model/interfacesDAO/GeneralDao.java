package model.interfacesDAO;

import java.util.List;

public interface GeneralDao<T, ID> {
    List<T> getAll();
    T getById(ID id);
    void create(T element);
    void update();

}
