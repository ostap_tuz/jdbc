package model.interfacesDAO;

import model.Vaccine;

public interface VaccineDao extends GeneralDao<Vaccine, Integer> {
}
