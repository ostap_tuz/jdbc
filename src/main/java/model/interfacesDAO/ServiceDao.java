package model.interfacesDAO;

import model.Service;

public interface ServiceDao extends GeneralDao<Service,Integer> {
}
