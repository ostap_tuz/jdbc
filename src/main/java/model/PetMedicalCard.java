package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

import java.util.Date;

@Table(name = "pet_medical_card")
public class PetMedicalCard {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private int id;
    @Column(name = "pet_age", length = 45)
    private String petAge;
    @Column(name = "pet_name", length = 45)
    private String petName;
    @Column(name = "date_ragistration", length = 20)
    private Date dateRegistration;
    @Column(name = "date_last_examination", length = 20)
    private Date date_last_examination;
    @Column(name = "client_id", length = 10)
    private int clientId;
    @Column(name = "pet_type_type", length = 45)
    private String petType;
    @Column(name = "pet_type_breed", length = 45)
    private String petBreed;

    public PetMedicalCard() {
    }

    public PetMedicalCard(int id, String petAge, String petName, Date dateRegistration, Date date_last_examination, int clientId, String petType, String petBreed) {
        this.id = id;
        this.petAge = petAge;
        this.petName = petName;
        this.dateRegistration = dateRegistration;
        this.date_last_examination = date_last_examination;
        this.clientId = clientId;
        this.petType = petType;
        this.petBreed = petBreed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPetAge() {
        return petAge;
    }

    public void setPetAge(String petAge) {
        this.petAge = petAge;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public Date getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(Date dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    public Date getDate_last_examination() {
        return date_last_examination;
    }

    public void setDate_last_examination(Date date_last_examination) {
        this.date_last_examination = date_last_examination;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    public String getPetBreed() {
        return petBreed;
    }

    public void setPetBreed(String petBreed) {
        this.petBreed = petBreed;
    }

    @Override
    public String toString() {
        return "PetMedicalCard{" +
                "id=" + id +
                ", petAge='" + petAge + '\'' +
                ", petName='" + petName + '\'' +
                ", dateRegistration=" + dateRegistration +
                ", date_last_examination=" + date_last_examination +
                ", clientId=" + clientId +
                ", petType='" + petType + '\'' +
                ", petBreed='" + petBreed + '\'' +
                '}';
    }
}
