package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

import java.util.Calendar;

@Table(name = "client")
public class Client {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private int id;
    @Column(name = "surname", length = 45)
    private String surname;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "visit_date", length = 30)
    private Calendar visitDate;
    @Column(name = "phone_number", length = 45)
    private String phoneNumber;

    public Client(){}

    public Client(int id, String surname, String name, Calendar visitDate, String phoneNumber) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.visitDate = visitDate;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Calendar visitDate) {
        this.visitDate = visitDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", visitDate=" + visitDate +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
