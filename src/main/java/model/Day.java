package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

@Table(name = "days")
public class Day {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private int id;
    @Column(name = "day_name", length = 45)
    private String dayName;

    public Day(){}

    public Day(int id, String dayName) {
        this.id = id;
        this.dayName = dayName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    @Override
    public String toString() {
        return "Day{" +
                "id=" + id +
                ", dayName='" + dayName + '\'' +
                '}';
    }
}
