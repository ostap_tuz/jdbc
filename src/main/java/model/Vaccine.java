package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

import java.util.Calendar;
import java.util.Date;

@Table(name = "vaccine")
public class Vaccine {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private int id;
    @Column(name = "vaccine_name", length = 45)
    private String vaccineName;
    @Column(name = "date_last_injection", length = 45)
    private Calendar dateLastInjection;
    @Column(name = "vaccine_price", length = 10)
    private String vaccinePrice;

    public Vaccine() {
    }

    public Vaccine(int id, String vaccineName, Calendar dateLastInjection, String vaccinePrice) {
        this.id = id;
        this.vaccineName = vaccineName;
        this.dateLastInjection = dateLastInjection;
        this.vaccinePrice = vaccinePrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public Calendar getDateLastInjection() {
        return dateLastInjection;
    }

    public void setDateLastInjection(Calendar dateLastInjection) {
        this.dateLastInjection = dateLastInjection;
    }

    public String getVaccinePrice() {
        return vaccinePrice;
    }

    public void setVaccinePrice(String vaccinePrice) {
        this.vaccinePrice = vaccinePrice;
    }

    @Override
    public String toString() {
        return "Vaccine{" +
                "id=" + id +
                ", vaccineName='" + vaccineName + '\'' +
                ", dateLastInjection=" + dateLastInjection +
                ", vaccinePrice='" + vaccinePrice + '\'' +
                '}';
    }
}
