package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

@Table(name = "doctor")
public class Doctor {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "surname", length = 45)
    private String surname;
    @Column(name = "profession", length = 45)
    private String profession;
    @Column(name = "phone_number", length = 45)
    private String phoneNumber;
    @Column(name = "room", length = 45)
    private String room;
    @Column(name = "doctor_schedule_id", length = 10)
    private String doctorScheduleId;

    public Doctor(){}

    public Doctor(int id, String name, String surname, String profession, String phoneNumber, String room, String doctorScheduleId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.profession = profession;
        this.phoneNumber = phoneNumber;
        this.room = room;
        this.doctorScheduleId = doctorScheduleId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDoctorScheduleId() {
        return doctorScheduleId;
    }

    public void setDoctorScheduleId(String doctorScheduleId) {
        this.doctorScheduleId = doctorScheduleId;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", profession='" + profession + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", room='" + room + '\'' +
                ", doctorScheduleId='" + doctorScheduleId + '\'' +
                '}';
    }
}
