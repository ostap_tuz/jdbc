package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

@Table(name = "service")
public class Service {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private int id;
    @Column(name = "service_name", length = 45)
    private String serviceName;
    @Column(name = "price", length = 10)
    private double price;

    public Service() {
    }

    public Service(int id, String serviceName, double price) {
        this.id = id;
        this.serviceName = serviceName;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", serviceName='" + serviceName + '\'' +
                ", price=" + price +
                '}';
    }
}
