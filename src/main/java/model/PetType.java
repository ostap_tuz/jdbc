package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

@Table(name = "pet_type")
public class PetType {
    @Column(name = "type", length = 45)
    private String type;
    @PrimaryKey
    @Column(name = "breed", length = 45)
    private String breed;

    public PetType() {
    }

    public PetType(String type, String breed) {
        this.type = type;
        this.breed = breed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Override
    public String toString() {
        return "PetType{" +
                "type='" + type + '\'' +
                ", breed='" + breed + '\'' +
                '}';
    }
}
