package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

import java.sql.Time;

@Table(name = "doctor_schedule")
public class DoctorSchedule {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private int id;
    @Column(name = "days_id", length = 10)
    private int days_id;
    @Column(name = "start_time", length = 25)
    private Time startTime;
    @Column(name = "finish_time", length = 25)
    private Time finishTime;
    @Column(name = "visit_id", length = 10)
    private int visit_id;

    public DoctorSchedule() {
    }

    public DoctorSchedule(int id, int days_id, Time startTime, Time finishTime, int visit_id) {
        this.id = id;
        this.days_id = days_id;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.visit_id = visit_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDays_id() {
        return days_id;
    }

    public void setDays_id(int days_id) {
        this.days_id = days_id;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Time finishTime) {
        this.finishTime = finishTime;
    }

    public int getVisit_id() {
        return visit_id;
    }

    public void setVisit_id(int visit_id) {
        this.visit_id = visit_id;
    }

    @Override
    public String toString() {
        return "DoctorSchedule{" +
                "id=" + id +
                ", days_id=" + days_id +
                ", startTime=" + startTime +
                ", finishTime=" + finishTime +
                ", visit_id=" + visit_id +
                '}';
    }
}
